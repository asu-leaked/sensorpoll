Programs
--------
 - Python 3.x
 - InfluxDB
 - [Humidity Reader](https://gitlab.com/asu-leaked/humidity-reader) cloned to the home directory

Python 3 Modules
----------------
 - influxdb
 - subprocess
 - time
