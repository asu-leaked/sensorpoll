# import needed modules
from influxdb import InfluxDBClient
import subprocess
from time import sleep

SLEEP_TIMEOUT = 0.2

# make a method to read from the humidity sensor
def getHumidity():
    # code goes here
    rawH = subprocess.Popen("/home/pi/Adafruit_Python_DHT/examples/AdafruitDHT.py 2302 22", shell=True, stdout=subprocess.PIPE).stdout.read().decode()
    processedH = rawH[:len(rawH)-1]
    return processedH

def sendToInflux(data):
    json_body = [
        {
            "measurement": "humidity",
            "fields": {
                "humidity": float(data)
            }
        }
    ]
    client = InfluxDBClient('localhost', 8086, 'root', 'root', 'sensor')
    client.write_points(json_body)
    del client

# make a function to run the whole program
def main():
    try:
        while True:
            humidity = getHumidity()
            print("Received \"%s\" from humidity sensor." % (humidity))
            sendToInflux(humidity)
            print("Data sent to influxDB.")
            sleep(SLEEP_TIMEOUT)
    except KeyboardInterrupt:
        print("OOF")
        exit()

# run the program
main()
